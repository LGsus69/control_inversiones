import { Injectable } from '@angular/core';
import { ResponseI } from '../modelos/response.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Info, Balance, Dashboard } from '../modelos/models.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //url: string = "https://api.itbook.store/1.0/";
  url: string = "https://619ba7e32782760017445720.mockapi.io/frontend-2021/";


  constructor( private http: HttpClient) { }

  getHistory(page: number): Observable<Info>{
    let direccion = this.url + "history-page" + page;
    return this.http.get<Info>(direccion);
  }

  getBalance(): Observable<Balance>{
    let direccion = this.url + "balance";
    return this.http.get<Balance>(direccion);
  }

  getDashboard(): Observable<Dashboard>{
    let direccion = this.url + "dashboard";
    return this.http.get<Dashboard>(direccion);
  }

}
