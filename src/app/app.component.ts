import { Component } from '@angular/core';
import { ResolveStart, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private router: Router,
    private menu: MenuController) { }

  dashboard() {
    this.menu.close();
    this.router.navigateByUrl('dashboard');
  }

  oportunidades() {
    this.menu.close();
    this.router.navigateByUrl('oportunidades');
  }

  inversiones() {
    this.menu.close();
    this.router.navigateByUrl('inversiones');
  }

  estadoCuenta() {  
    this.menu.close();
    this.router.navigateByUrl('estado-cuenta');
  }

}
