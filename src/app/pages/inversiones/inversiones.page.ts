import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api.service';

@Component({
  selector: 'app-inversiones',
  templateUrl: './inversiones.page.html',
  styleUrls: ['./inversiones.page.scss'],
})
export class InversionesPage implements OnInit {

  balance: any;
  resultados: any;
  depositos_totales: any;
  retiros_totales: any;
  inversiones_totales: any;
  ganancias_totales: any;

  constructor( private api: ApiService ) { }

  ngOnInit(): void {
    this.getLista();
    this.getListe();
  }

  getLista() {
    this.api.getBalance()
       .subscribe(data => {
        this.balance = data.available;
          console.log('Lista balances: ',  this.balance);
       })
 }

 getListe() {
  this.api.getDashboard()
     .subscribe(data => {
        this.resultados = data;
        this.depositos_totales = data.deposits_amount;
        this.retiros_totales = data.withdrawals_amount;
        this.inversiones_totales = data.repaid_bids.paid;
        this.ganancias_totales = data.repaid_bids.profit;
        console.log('this.depositos_totales: ', this.ganancias_totales );
     })
}

}
