import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ApiService } from '../../servicios/api.service';
import { Router } from '@angular/router';
import { Info, Result } from '../../modelos/models.interface';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-estado-cuenta',
  templateUrl: './estado-cuenta.page.html',
  styleUrls: ['./estado-cuenta.page.scss'],
})
export class EstadoCuentaPage implements OnInit {

  image: any;
  title: any;
  price: any;

  pageSize: any = 10;
  page: any = 1;
  desde: number = 0;
  hasta: number = 9;
  cant: any;

  amount: any;

  transaction_type: any[] = [];
  movimiento: any;
  name: any;
  color: any;

  filterBook = '';
  informacion: Info = { count: 0, next: null, previous: '', results: [], first_balance: 0 };
  resultados: Result[] = [];





  constructor(private api: ApiService,
     private router: Router) {

  }


  ngOnInit(): void {
    this.getLista(this.page);

 }

 getLista(pagina: any) {
    this.api.getHistory(pagina)
       .subscribe(data => {
          this.resultados = data.results;
          this.informacion = data;
          this.cant = this.informacion.count;
          console.log('Lista resultados: ', data);
       })
 }

 getMonto(monto: any){
   this.informacion;
   console.log('GET MONTO ' , this.informacion.first_balance );
   if( this.informacion.first_balance >= 0){
       this.amount = monto + this.informacion.first_balance;
       console.log('MONTO POSITIVO'); 
   } else {
     this.amount = monto - this.informacion.first_balance;
     console.log('MONTO NEGATIVO');
   }
 }

 getMovimientos(type: any, transaction: any ) {
    switch (type === 'BalanceTransaction') {
      case transaction === 1:{
          this.movimiento = 'Deposito';
          this.name = 'ico-deposito.png';
       break;
      }
       case transaction === 2:{
          this.movimiento = 'Retiro';
          this.name = 'ico-retiro.png';
       break;
       }
       case transaction === 3:{
          this.movimiento = 'Ingreso Extraordinario';
          this.name = 'ico-ingreso-extraordinario.png';
          break;
      }
       case transaction === 4:{
          this.movimiento = 'Retención de impuestos';
          this.name = 'ico-retencion-impuestos.png';
          break;
       }

      } 
      switch (type === 'BidTransaction'){
       case transaction === 1:{
          this.movimiento = 'Inversión';
          this.name = 'ico-inversion.png';
       break;
       }
       case transaction === 2:{
           this.movimiento = 'Devolución';
           this.name = 'ico-devolucion.png';
       break;
        }
       case transaction === 3:{
           this.movimiento = 'Repago';
           this.name = 'ico-repago.png';
       break;
       }
       case transaction === 4:{
           this.movimiento = 'Siniestro';
           this.name = 'ico-siniestro.png';
       break;
        }
      }    
 }
}
