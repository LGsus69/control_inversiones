export interface Info {
    count:         number;
    next:          null;
    previous:      string;
    results:       Result[];
    first_balance: number;
}

export interface Result {
    id:               string;
    status:           number;
    transaction_type: number;
    amount:           string;
    model_type:       string;
    creation_dt:      Date;
    end_dt:           Date;
    operation:        null | string;
    auction:          number | null;
}

export interface Balance {
    available: string;
}

export interface Dashboard {
    repaid_bids:        RepaidBids;
    deposits_amount:    number;
    withdrawals_amount: number;
}

export interface RepaidBids {
    paid:   number;
    profit: number;
}